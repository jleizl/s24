// S24 Activity Template:
/*Item 1.)
	- Create a variable getCube and use the exponent operator to compute the cube of a number. (A cube is any number raised to 3)
	- Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
	*/

// Code here:



function numberCube(num) {
    let thirdPower = num ** 3;
    console.log(`The cube of ${num} is ${thirdPower}`)
}

let getCube = numberCube;
getCube(2)


/*Item 2.)
	- Create a variable address with a value of an array containing details of an address.
	- Destructure the array and print out a message with the full address using Template Literals.*/


// Code here:

let address = ["Quezon Avenue", "Brgy. 5", "Catanauan", "Quezon"]
const [streetName, subdivision, municupality, province] = address
console.log(`I live at ${streetName}, ${subdivision}, ${municupality}, ${province}`)


/*Item 3.)
		- Create a variable animal with a value of an object data type with different animal details as its properties.
		- Destructure the object and print out a message with the details of the animal using Template Literals.
*/
// Code here:

const animal = {
    animalType: "Martial Eagle",
    located: "South Africa",
    wingspan: "8.5-foot",
    weight: "14 pounds"
}

const { animalType, located, wingspan, weight } = animal;

function animalProfile(animalType, located, wingspan, weight) {
    console.log(`The ${animalType} lives in ${located}. It has ${wingspan} wingspan and weighed ${weight}. It is the biggest eagle alive today.`)
}

animalProfile(animalType, located, wingspan, weight)

/*Item 4.)
	- Create an array of numbers.
	- Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.*/

// Code here:


let numbers = [2, 4, 6, 8, 10]
numbers.forEach((number) => {
    console.log(`${number}`)
})

const getNumber = (num) => {
	return `${num}`
}

console.log(getNumber(15))
console.log(getNumber(20))



/*	Item 5.
		- Create a class of a Dog and a constructor that will accept a name, age and breed as its properties.
		- Create/instantiate a new object from the class Dog and console log the object.*/

// Code here:*/

class Dog {
    constructor(name, age, breed) {
        this.name = name
        this.age = age
        this.breed = breed
    }
}

const petDog = new Dog()

petDog.name = "Wally"
petDog.age = "5"
petDog.breed = "Chao Chao"

console.log(petDog)